# Bambinifashion-test

#### Description

Test exercise

#### Requirements
- MacOS or Linux 
- Docker
- Docker-compose


#### Installation

Execute a command from the root directory of a project 
``` 
$ make 
```

#### Usage

Go to URL http://172.172.172.4/


#### Stripe test cards
https://stripe.com/docs/testing

```
4000000000000077
any random CVC
a valid expiration date in the future 
```
