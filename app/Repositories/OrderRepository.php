<?php

namespace App\Repositories;

use App\Http\Controllers\CartController;
use App\Http\Requests\PlaceOrderRequest;
use App\Jobs\ChargeUser;
use App\Mail\OrderPlaced;
use App\Order;
use App\Product;
use App\ShippingOption;
use App\User;
use Mail;

/**
 * Class OrderRepository
 */
class OrderRepository
{
    /**
     * @param PlaceOrderRequest $request
     * @return Order
     */
    public function placeOrder(PlaceOrderRequest $request): Order
    {
        /** @var User $user */
        $user = $request->user();
        $product = Product::findOrFail($request->session()->get(CartController::PRODUCT_SESSION_KEY));
        $shippingOption = ShippingOption::findOrFail($request->get('shipping-option'));
        $amount = ($product->price + $shippingOption->cost) * 100;
        $paymentMethod = $request->get('payment-method');

        // Put user charge to a queue
        ChargeUser::dispatch($user, $paymentMethod, $amount);

        // Persist order in DB
        $order = new Order();

        $order->total_product_value = $product->price;
        $order->total_shipping_value = $shippingOption->cost;
        $order->client_name = $request->get('name');
        $order->client_address = $request->get('address');

        $order->save();

        // Put an email to a queue
        Mail::to($request->user())
            ->bcc(env('ADMIN_EMAIL'))
            ->queue(new OrderPlaced($order));

        return $order;
    }
}
