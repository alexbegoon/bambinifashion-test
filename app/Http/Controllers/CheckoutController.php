<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlaceOrderRequest;
use App\Product;
use App\Repositories\OrderRepository;
use App\ShippingOption;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class CheckoutController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * CheckoutController constructor.
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param Request $request
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function index(Request $request)
    {
        if (!$request->session()->has(CartController::PRODUCT_SESSION_KEY)) {
            return redirect('home');
        }

        $product = Product::findOrFail($request->session()->get(CartController::PRODUCT_SESSION_KEY));
        $shippingOptions = ShippingOption::all();
        $intent = $request->user()->createSetupIntent();

        return view('checkout', compact('product', 'shippingOptions', 'intent'));
    }

    /**
     * @param PlaceOrderRequest $request
     * @return RedirectResponse|Redirector
     */
    public function placeOrder(PlaceOrderRequest $request)
    {
        try {
            $this->orderRepository->placeOrder($request);
        } catch (\Throwable $throwable) {
            $request->session()->flash('error', $throwable->getMessage());
            return redirect()->back();
        }

        return redirect('thank-you');
    }

    /**
     * @return Factory|View
     */
    public function thankYou()
    {
        return view('thank-you');
    }
}
