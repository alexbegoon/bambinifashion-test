<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        /** @var LengthAwarePaginator $products */
        $products = Product::with(['brand'])->paginate(env('PRODUCTS_PER_PAGE'));

        return view('products', compact('products'));
    }
}
