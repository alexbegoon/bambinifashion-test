<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddToCartRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class CartController extends Controller
{
    public const PRODUCT_SESSION_KEY = 'product_id';

    /**
     * @param AddToCartRequest $request
     * @return RedirectResponse|Redirector
     */
    public function addToCart(AddToCartRequest $request)
    {
        // Add single product to session
        $request->session()->put(self::PRODUCT_SESSION_KEY, $request->get(self::PRODUCT_SESSION_KEY));
        return redirect('checkout');
    }
}
