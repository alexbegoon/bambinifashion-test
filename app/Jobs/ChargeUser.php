<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ChargeUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $paymentMethod;
    /**
     * @var float
     */
    private $amount;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param string $paymentMethod
     * @param float $amount
     */
    public function __construct(User $user, string $paymentMethod, float $amount)
    {
        $this->user = $user;
        $this->paymentMethod = $paymentMethod;
        $this->amount = $amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        if (!$this->user->hasStripeId()) {
            $this->user->createAsStripeCustomer();
        }
        $this->user->addPaymentMethod($this->paymentMethod);
        $this->user->save();
        // Charge user
        $this->user->charge($this->amount, $this->paymentMethod);
    }
}
