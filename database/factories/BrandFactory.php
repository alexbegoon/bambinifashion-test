<?php

/** @var Factory $factory */

use App\Brand;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Brand::class, static function (Faker $faker) {
    return [
        'name' => $faker->company
    ];
});
