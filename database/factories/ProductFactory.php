<?php

/** @var Factory $factory */

use App\Brand;
use App\Product;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Product::class, static function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'price' => $faker->randomFloat(2, 10, 1000),
        'brand_id' => \factory(Brand::class)
    ];
});
