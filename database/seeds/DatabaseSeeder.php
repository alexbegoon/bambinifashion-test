<?php

use App\Product;
use App\ShippingOption;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Seed products
        factory(Product::class, 1000)->create();

        // Seed shipping options
        $shippingOption = new ShippingOption();
        $shippingOption->name = 'Free Standard';
        $shippingOption->cost = 0;
        $shippingOption->save();

        $shippingOption = new ShippingOption();
        $shippingOption->name = 'Express';
        $shippingOption->cost = 10;
        $shippingOption->save();
    }
}
