@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{__('main.checkout')}}</div>

                    <div class="card-body">
                        <span>{{__('main.you_are_about_to_order')}}
                            <strong>{{$product->name}} - {{$product->price}} EUR</strong>
                        </span>
                        <br>
                        <br>
                        <span>{{__('main.please_provide_order_details')}}</span>
                        <br>
                        <br>
                        <form action="{{route('place-an-order')}}" method="POST">
                            {{csrf_field()}}
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="field-name">Name</label>
                                        <input required id="field-name" name="name"
                                               value="{{old('name', Auth::user()->name)}}"
                                               class="form-control @error('name') is-invalid @enderror" type="text"/>
                                        @error('name')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="field-address">Address</label>
                                        <input required id="field-address" name="address" value="{{old('address')}}"
                                               class="form-control @error('address') is-invalid @enderror"
                                               type="text"/>
                                        @error('address')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="field-shipping-option">Shipping Option</label>
                                        <select required id="field-shipping-option" name="shipping-option"
                                                class="form-control @error('shipping-option') is-invalid @enderror">
                                            @foreach($shippingOptions as $shippingOption)
                                                <option value="{{$shippingOption->id}}">{{$shippingOption->name}}
                                                    - {{$shippingOption->cost}} EUR
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('shipping-option')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label for="card-holder-name">Cardholder</label>
                                        <input id="card-holder-name" type="text" class="form-control @error('payment-method') is-invalid @enderror"">
                                        @error('payment-method')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <!-- Stripe Elements Placeholder -->
                                        <div id="card-element"></div>
                                    </div>

                                    <div>
                                        <input type="hidden" id="payment-method" name="payment-method" value="" />
                                        <button type="button" id="card-button" class="btn btn-info"
                                                data-secret="{{ $intent->client_secret }}">
                                            Add Card
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group mt-5">
                                <button class="btn btn-primary" type="submit">{{__('main.place_an_order')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        const stripe = Stripe('{{env('STRIPE_KEY')}}');

        const elements = stripe.elements();
        const cardElement = elements.create('card');

        cardElement.mount('#card-element');

        const cardHolderName = document.getElementById('card-holder-name');
        const cardButton = document.getElementById('card-button');
        const paymentMethodField = document.getElementById('payment-method');

        cardButton.addEventListener('click', async (e) => {
            const {paymentMethod, error} = await stripe.createPaymentMethod(
                'card', cardElement, {
                    billing_details: {name: cardHolderName.value}
                }
            );

            if (error) {
                // Display "error.message" to the user...
            } else {
                paymentMethodField.value = paymentMethod.id
            }
        });
    </script>
@endsection
