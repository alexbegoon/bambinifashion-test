@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Products</div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Brand</th>
                                <th scope="col">Name</th>
                                <th scope="col">Price</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <th scope="row">{{$product->brand->name}}</th>
                                    <td>{{$product->name}}</td>
                                    <td>{{number_format($product->price, 2)}}</td>
                                    <td>
                                        <form method="POST" action="{{route('add-to-cart')}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="product_id" value="{{$product->id}}" />
                                            <button type="submit" class="btn btn-info">Buy</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $products->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
