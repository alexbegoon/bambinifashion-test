@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{__('main.thank_you')}}</div>

                    <div class="card-body">
                        <span>{{__('main.your_order_has_been_placed')}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
