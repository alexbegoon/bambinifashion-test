<h4>Order details: </h4>
<br>
<br>
<ul>
    <li>Total product value: {{$order->total_product_value}}</li>
    <li>Total shipping value: {{$order->total_shipping_value}}</li>
    <li>Client name: {{$order->client_name}}</li>
    <li>Client address: {{$order->client_address}}</li>
</ul>
