<?php

return [
    'checkout' => 'Checkout',
    'you_are_about_to_order' => 'You are about to order',
    'please_provide_order_details' => 'Please provide order details',
    'place_an_order' => 'Place an order',
    'your_order_has_been_placed' => 'Your order has been placed',
    'thank_you' => 'Thank you!',
];
