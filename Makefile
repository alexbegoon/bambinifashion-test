.PHONY: all

all: prepare_infrastructure prepare_app prepare_app_conf prepare_database prepare_supervisor

prepare_infrastructure:
	@echo "Prepare Dockerized Infrastructure..."
	docker-compose pull
	docker-compose build
	docker-compose up -d
	@echo "Prepare Dockerized Infrastructure - OK"

prepare_app:
	@echo "Prepare App..."
	docker-compose exec php-fpm composer install
	docker-compose exec node npm install
	docker-compose exec node npm run production
	@echo "Prepare App - OK"

prepare_app_conf:
	@echo "Prepare App Configuration..."
	cp .env.example .env
	docker-compose exec php-fpm php artisan key:generate
	@echo "Prepare App Configuration - OK"

prepare_database:
	@echo "Prepare Database..."
	docker-compose exec php-fpm php artisan migrate:fresh --seed
	@echo "Prepare Database - OK"

ide_help:
	@echo "Prepare IDE Help..."
	docker-compose exec php-fpm php artisan ide-helper:generate
	docker-compose exec php-fpm php artisan ide-helper:models -N
	docker-compose exec php-fpm php artisan ide-helper:meta
	@echo "Prepare IDE Help - OK"

prepare_supervisor:
	@echo "Prepare Supervisor..."
	docker-compose exec php-fpm supervisord -c /etc/supervisord.conf
	docker-compose exec php-fpm supervisorctl reread
	docker-compose exec php-fpm supervisorctl update
	docker-compose exec php-fpm supervisorctl start laravel-worker:*
	docker-compose exec php-fpm php artisan queue:restart
	@echo "Prepare Supervisor - OK"
