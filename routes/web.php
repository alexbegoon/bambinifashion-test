<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::redirect('/', '/home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products', 'ProductController@index')->name('products');
Route::get('/checkout', 'CheckoutController@index')->name('checkout');
Route::post('/add-to-cart', 'CartController@addToCart')->name('add-to-cart');
Route::post('/place-an-order', 'CheckoutController@placeOrder')->name('place-an-order');
Route::get('/thank-you', 'CheckoutController@thankYou')->name('thank-you');
